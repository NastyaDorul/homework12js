
document.addEventListener('click', function (e) {
    let UserDiameter=document.createElement('input');
    UserDiameter.placeholder='Enter circle diameter';
    UserDiameter.id="circle-diameter";

    let UserColor=document.createElement('input');
    UserColor.placeholder='Enter circle color';
    UserColor.id="circle-color";

    let DrawBttn=document.createElement('button');
    DrawBttn.innerText="Draw Circle!";
    DrawBttn.id="Draw";

    if(e.target.id==="add-circles") {
        document.body.appendChild(UserDiameter);
        document.body.appendChild(UserColor);
        document.body.appendChild(DrawBttn);
        document.getElementById('add-circles').style.display='none';
        const container=document.createElement('div');
        container.id='circle-container';
        document.body.appendChild(container);
    }
    if(e.target.id==="Draw"){
    const number = +document.getElementById("circle-diameter").value;
    console.log(number+'px');
    const color = +document.getElementById("circle-color").value;
    console.log('#'+color);
    const item = document.createElement("div");
    const itemAttr=item.style;
    item.classList.toggle('item');
    itemAttr.width=number+'px';
    itemAttr.height=number+'px';
    itemAttr.backgroundColor='#'+color;
    const insert=document.getElementById('circle-container');
    insert.appendChild(item);
    insert.appendChild(item);
    document.getElementById('circle-diameter').value = '';
    document.getElementById('circle-color').value = '';
    }
});



